package ar.edu.undec.adapter.data.tipopersona.repoimplementacion;

import ar.edu.undec.adapter.data.tipopersona.crud.ConsultarTipoPersonaPorIdCRUD;
import ar.edu.undec.adapter.data.tipopersona.mapper.TipoPersonaDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tipopersona.model.TipoPersona;
import tipopersona.output.ConsultarTipoPersonaPorIdRepository;

@Service
public class ConstularTipoPersonaPorIdRepoImplementacion implements ConsultarTipoPersonaPorIdRepository {

    private ConsultarTipoPersonaPorIdCRUD consultarTipoPersonaPorIdCRUD;

    @Autowired
    public ConstularTipoPersonaPorIdRepoImplementacion(ConsultarTipoPersonaPorIdCRUD consultarTipoPersonaPorIdCRUD) {
        this.consultarTipoPersonaPorIdCRUD = consultarTipoPersonaPorIdCRUD;
    }

    @Override
    public TipoPersona consultarTipoPersonaPorId(Integer tipoPersonaId) {
        return TipoPersonaDataMapper.dataCoreMapper(consultarTipoPersonaPorIdCRUD.findById(tipoPersonaId).get());
    }
}
