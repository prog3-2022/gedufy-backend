package ar.edu.undec.adapter.data.persona.repoimplementacion;

import ar.edu.undec.adapter.data.persona.crud.CrearPersonaCRUD;
import ar.edu.undec.adapter.data.persona.mapper.PersonaDataMapper;
import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persona.model.Persona;
import persona.output.CrearPersonaRepository;

@Service
public class CrearPersonaRepoImplementacion implements CrearPersonaRepository {

    private CrearPersonaCRUD crearPersonaCRUD;

    @Autowired
    public CrearPersonaRepoImplementacion(CrearPersonaCRUD crearPersonaCRUD) {
        this.crearPersonaCRUD = crearPersonaCRUD;
    }

    @Override
    public boolean existePorDNI(String dni) {
        return crearPersonaCRUD.existsByDni(dni);
    }

    @Override
    public boolean existePorEmail(String email) {
        return crearPersonaCRUD.existsByEmail(email);
    }

    @Override
    public Integer guardarPersona(Persona persona) {
        try {
            return crearPersonaCRUD.save(PersonaDataMapper.dataEntityMapper(persona)).getId();
        } catch (RuntimeException e) {
            return 0;
        }
    }
}
