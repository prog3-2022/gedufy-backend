package ar.edu.undec.adapter.service.inscripcion.controller;

import inscripcion.exception.InscripcionYaExisteException;
import inscripcion.input.CrearInscripcionInput;
import inscripcion.usecase.crearinscripcionusecase.CrearInscripcionRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("inscripciones")
public class CrearInscripcionController {

    private CrearInscripcionInput crearInscripcionInput;

    @Autowired
    public CrearInscripcionController(CrearInscripcionInput crearInscripcionInput) {
        this.crearInscripcionInput = crearInscripcionInput;
    }

    @PostMapping
    public ResponseEntity<?> crearInscripcion(@RequestBody CrearInscripcionRequestModel crearInscripcionRequestModel) {
        try {
            return ResponseEntity.created(null).body(crearInscripcionInput.crearInscripcion(crearInscripcionRequestModel));
        } catch (InscripcionYaExisteException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
