package ar.edu.undec.adapter.data.inscripcion.crud;

import ar.edu.undec.adapter.data.curso.model.CursoEntity;
import ar.edu.undec.adapter.data.inscripcion.model.InscripcionEntity;
import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.data.repository.CrudRepository;

public interface CrearInscripcionCRUD extends CrudRepository<InscripcionEntity, Integer> {
    //TODO: Usar Underscore para separar atributos de tipo definido por el usuario, y llegar al atributo atómico necesario
    // https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-property-expressions
    Boolean existsByPersonaAndCurso(PersonaEntity personaId, CursoEntity cursoId);
}
