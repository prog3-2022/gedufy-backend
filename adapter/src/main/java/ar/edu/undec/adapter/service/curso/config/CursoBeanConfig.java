package ar.edu.undec.adapter.service.curso.config;

import curso.output.ConsultarCursoPorIdRepositorio;
import curso.output.ConsultarCursosRepositorio;
import curso.usecase.consultarcursoporidusecase.ConsultarCursoPorIdUseCase;
import curso.usecase.consultarcursosusecase.ConsultarCursosUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CursoBeanConfig {

    @Bean
    public ConsultarCursosUseCase consultarCursosUseCase(ConsultarCursosRepositorio consultarCursosRepositorio) {
        return new ConsultarCursosUseCase(consultarCursosRepositorio);
    }

    @Bean
    public ConsultarCursoPorIdUseCase consultarCursoPorIdUseCase(ConsultarCursoPorIdRepositorio consultarCursoPorIdRepositorio) {
        return new ConsultarCursoPorIdUseCase(consultarCursoPorIdRepositorio);
    }
}
