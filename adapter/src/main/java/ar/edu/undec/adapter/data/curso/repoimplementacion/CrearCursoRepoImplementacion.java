package ar.edu.undec.adapter.data.curso.repoimplementacion;

import ar.edu.undec.adapter.data.curso.crud.CrearCursoCRUD;
import ar.edu.undec.adapter.data.curso.mapper.CursoDataMapper;
import curso.model.Curso;
import curso.output.CrearCursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrearCursoRepoImplementacion implements CrearCursoRepository {

    private CrearCursoCRUD crearCursoCRUD;

    @Autowired
    public CrearCursoRepoImplementacion(CrearCursoCRUD crearCursoCRUD) {
        this.crearCursoCRUD = crearCursoCRUD;
    }

    @Override
    public Integer guardarCurso(Curso curso) {
        return crearCursoCRUD.save(CursoDataMapper.dataEntityMapper(curso)).getId();
    }
}
