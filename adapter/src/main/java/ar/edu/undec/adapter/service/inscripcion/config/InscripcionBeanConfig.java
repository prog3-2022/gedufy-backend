package ar.edu.undec.adapter.service.inscripcion.config;

import curso.usecase.consultarcursoporidusecase.ConsultarCursoPorIdUseCase;
import inscripcion.output.CrearInscripcionRepository;
import inscripcion.usecase.crearinscripcionusecase.CrearInscripcionUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import persona.usecase.consultarpersonaporidusecase.ConsultarPersonaPorIdUseCase;

@Configuration
public class InscripcionBeanConfig {

    @Bean
    public CrearInscripcionUseCase crearInscripcionUseCase(CrearInscripcionRepository crearInscripcionRepository,
                                                           ConsultarPersonaPorIdUseCase consultarPersonaPorIdUseCase,
                                                           ConsultarCursoPorIdUseCase consultarCursoPorIdUseCase) {
        return new CrearInscripcionUseCase(crearInscripcionRepository, consultarPersonaPorIdUseCase, consultarCursoPorIdUseCase);
    }
}
