package ar.edu.undec.adapter.service.tipopersona.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tipopersona.usecase.consultartipospersonausecase.ConsultarTiposPersonaResponseModel;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("tipospersona")
public class ConsultarTiposPersonaController {

    @GetMapping
    public ResponseEntity<?> consultarTiposPersona() {
        try {
            // TODO: 29/10/2022 - Reemplazar mock
            List<ConsultarTiposPersonaResponseModel> consultarTiposPersonaResponseModelList = new ArrayList<>();
            consultarTiposPersonaResponseModelList.add(ConsultarTiposPersonaResponseModel.instancia(1, "Alumno"));
            consultarTiposPersonaResponseModelList.add(ConsultarTiposPersonaResponseModel.instancia(2, "Profesor"));
            consultarTiposPersonaResponseModelList.add(ConsultarTiposPersonaResponseModel.instancia(3, "Otro"));

            return ResponseEntity.ok(consultarTiposPersonaResponseModelList);

        } catch (Exception e) {
            return ResponseEntity.noContent().build();
        }
    }
}
