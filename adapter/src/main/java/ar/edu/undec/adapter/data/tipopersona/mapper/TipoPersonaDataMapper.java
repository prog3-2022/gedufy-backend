package ar.edu.undec.adapter.data.tipopersona.mapper;

import ar.edu.undec.adapter.data.tipopersona.model.TipoPersonaEntity;
import tipopersona.model.TipoPersona;

public class TipoPersonaDataMapper {

    public static TipoPersona dataCoreMapper(TipoPersonaEntity tipoPersonaEntity) {
        return TipoPersona.instancia(tipoPersonaEntity.getId(),
                tipoPersonaEntity.getNombre(),
                tipoPersonaEntity.getDescripcion());
    }

    public static TipoPersonaEntity dataEntityMapper(TipoPersona tipoPersona) {
        TipoPersonaEntity tipoPersonaEntity = new TipoPersonaEntity(tipoPersona.getId(),
                tipoPersona.getNombre(),
                tipoPersona.getDescripcion());
        return tipoPersonaEntity;
    }
}
