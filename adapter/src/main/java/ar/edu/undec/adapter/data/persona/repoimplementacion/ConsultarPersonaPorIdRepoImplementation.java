package ar.edu.undec.adapter.data.persona.repoimplementacion;

import ar.edu.undec.adapter.data.persona.crud.ConsultarPersonaPorIdCRUD;
import ar.edu.undec.adapter.data.persona.mapper.PersonaDataMapper;
import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persona.model.Persona;
import persona.output.ConsultarPersonaPorIdRepository;

@Service
public class ConsultarPersonaPorIdRepoImplementation implements ConsultarPersonaPorIdRepository {

    private ConsultarPersonaPorIdCRUD consultarPersonaPorIdCRUD;

    @Autowired
    public ConsultarPersonaPorIdRepoImplementation(ConsultarPersonaPorIdCRUD consultarPersonaPorIdCRUD) {
        this.consultarPersonaPorIdCRUD = consultarPersonaPorIdCRUD;
    }

    @Override
    public Persona consultarPersonaPorId(Integer id) {
        try {
            PersonaEntity personaEntity = consultarPersonaPorIdCRUD.findPersonaEntityById(id);
            return PersonaDataMapper.dataCoreMapper(personaEntity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
