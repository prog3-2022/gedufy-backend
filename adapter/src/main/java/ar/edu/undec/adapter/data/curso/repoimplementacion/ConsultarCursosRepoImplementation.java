package ar.edu.undec.adapter.data.curso.repoimplementacion;

import ar.edu.undec.adapter.data.curso.crud.ConsultarCursosCRUD;
import ar.edu.undec.adapter.data.curso.mapper.CursoDataMapper;
import ar.edu.undec.adapter.data.curso.model.CursoEntity;
import curso.model.Curso;
import curso.output.ConsultarCursosRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConsultarCursosRepoImplementation implements ConsultarCursosRepositorio {

    private ConsultarCursosCRUD consultarCursosCRUD;

    @Autowired
    public ConsultarCursosRepoImplementation(ConsultarCursosCRUD consultarCursosCRUD) {
        this.consultarCursosCRUD = consultarCursosCRUD;
    }

    @Override
    public Collection<Curso> obtenerCursos() {
        try {
            return consultarCursosCRUD.findAll().stream().map(CursoDataMapper::dataCoreMapper).collect(Collectors.toCollection(ArrayList::new));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
