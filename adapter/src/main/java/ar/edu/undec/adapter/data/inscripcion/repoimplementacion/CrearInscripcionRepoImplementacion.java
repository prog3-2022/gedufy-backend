package ar.edu.undec.adapter.data.inscripcion.repoimplementacion;

import ar.edu.undec.adapter.data.curso.crud.ConsultarCursoPorIdCRUD;
import ar.edu.undec.adapter.data.inscripcion.crud.CrearInscripcionCRUD;
import ar.edu.undec.adapter.data.inscripcion.mapper.InscripcionDataMapper;
import ar.edu.undec.adapter.data.persona.crud.ConsultarPersonaPorIdCRUD;
import inscripcion.model.Inscripcion;
import inscripcion.output.CrearInscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persona.input.ConsultarPersonaPorIdInput;

@Service
public class CrearInscripcionRepoImplementacion implements CrearInscripcionRepository {

    private CrearInscripcionCRUD crearInscripcionCRUD;
    //TODO: Priorizar la reutilización de UseCase si la funcionalidad está hecha en Core.
    // Evitar el uso de un CRUD igual, ya que eso "obligaría" a testear los escenarios posibles de ese CRUD
    // Al tener hecho el UseCase, esos escenarios ya estan testeados.
    private ConsultarPersonaPorIdInput consultarPersonaPorIdInput;

    private ConsultarPersonaPorIdCRUD consultarPersonaPorIdCRUD;
    private ConsultarCursoPorIdCRUD consultarCursoPorIdCRUD;

    @Autowired
    public CrearInscripcionRepoImplementacion(CrearInscripcionCRUD crearInscripcionCRUD,
                                              ConsultarPersonaPorIdCRUD consultarPersonaPorIdCRUD,
                                              ConsultarCursoPorIdCRUD consultarCursoPorIdCRUD) {
        this.crearInscripcionCRUD = crearInscripcionCRUD;
        this.consultarPersonaPorIdCRUD = consultarPersonaPorIdCRUD;
        this.consultarCursoPorIdCRUD = consultarCursoPorIdCRUD;
    }

    @Override
    public boolean existePorPersonaAndCurso(Integer personaId, Integer cursoId) {
        return crearInscripcionCRUD.existsByPersonaAndCurso(
                consultarPersonaPorIdCRUD.findById(personaId).get(),
                consultarCursoPorIdCRUD.findById(cursoId).get());
    }

    @Override
    public Integer guardarInscripcion(Inscripcion inscripcion) {
        return crearInscripcionCRUD.save(InscripcionDataMapper.dataEntityMapper(inscripcion)).getId();
    }
}
