package ar.edu.undec.adapter.data.categoria.mapper;

import ar.edu.undec.adapter.data.categoria.model.CategoriaEntity;
import categoria.model.Categoria;

public class CategoriaDataMapper {

    public static Categoria dataCoreMapper(CategoriaEntity categoriaEntity) {
        return Categoria.instancia(categoriaEntity.getId(),
                categoriaEntity.getNombre(),
                categoriaEntity.getDescripcion());
    }

    public static CategoriaEntity dataEntityMapper(Categoria categoria) {
        CategoriaEntity categoriaEntity = new CategoriaEntity(categoria.getId(),
                categoria.getNombre(),
                categoria.getDescripcion());
        return categoriaEntity;
    }
}
