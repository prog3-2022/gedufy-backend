package ar.edu.undec.adapter.data.curso.repoimplementacion;

import ar.edu.undec.adapter.data.curso.crud.ConsultarCursoPorIdCRUD;
import ar.edu.undec.adapter.data.curso.mapper.CursoDataMapper;
import ar.edu.undec.adapter.data.curso.model.CursoEntity;
import curso.model.Curso;
import curso.output.ConsultarCursoPorIdRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsultarCursoPorIdRepoImplementation implements ConsultarCursoPorIdRepositorio {

    private ConsultarCursoPorIdCRUD consultarCursoPorIdCRUD;

    @Autowired
    public ConsultarCursoPorIdRepoImplementation(ConsultarCursoPorIdCRUD consultarCursoPorIdCRUD) {
        this.consultarCursoPorIdCRUD = consultarCursoPorIdCRUD;
    }

    @Override
    public Curso obtenerCursoPorId(Integer id) {
        try {
            CursoEntity cursoEntity = consultarCursoPorIdCRUD.findCursoEntityById(id);
            return CursoDataMapper.dataCoreMapper(cursoEntity);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
