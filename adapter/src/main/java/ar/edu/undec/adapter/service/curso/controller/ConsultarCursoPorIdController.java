package ar.edu.undec.adapter.service.curso.controller;

import curso.input.ConsultarCursoPorIdInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("cursos")
public class ConsultarCursoPorIdController {

    private ConsultarCursoPorIdInput consultarCursoPorIdInput;

    @Autowired
    public ConsultarCursoPorIdController(ConsultarCursoPorIdInput consultarCursoPorIdInput) {
        this.consultarCursoPorIdInput = consultarCursoPorIdInput;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> consultarCursoPorId(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(consultarCursoPorIdInput.obtenerCursoPorId(id));
        } catch (Exception e) {
            return ResponseEntity.noContent().build();
        }
    }
}
