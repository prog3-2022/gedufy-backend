package ar.edu.undec.adapter.data.persona.model;

import ar.edu.undec.adapter.data.tipopersona.model.TipoPersonaEntity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "persona")
public class PersonaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String dni;
    private String nombre;
    private String email;
    private String telefono;
    private LocalDate fechaNacimiento;
    @JoinColumn(name = "tipo_persona_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoPersonaEntity tipoPersona;

    public PersonaEntity() {
    }

    public PersonaEntity(Integer id, String dni, String nombre, String email, String telefono, LocalDate fechaNacimiento, TipoPersonaEntity tipoPersona) {
        this.id = id;
        this.dni = dni;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.tipoPersona = tipoPersona;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public TipoPersonaEntity getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(TipoPersonaEntity tipoPersona) {
        this.tipoPersona = tipoPersona;
    }
}
