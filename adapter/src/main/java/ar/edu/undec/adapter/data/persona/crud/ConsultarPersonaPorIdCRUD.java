package ar.edu.undec.adapter.data.persona.crud;

import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.data.repository.CrudRepository;

public interface ConsultarPersonaPorIdCRUD extends CrudRepository<PersonaEntity, Integer> {
    PersonaEntity findPersonaEntityById(Integer id);
}
