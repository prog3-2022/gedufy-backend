package factory;

import curso.model.Curso;

import java.util.ArrayList;
import java.util.List;

public class CursoFactory {

    private CursoFactory() {
    }

    public static Curso factoryCurso(Integer id) {
        return Curso.instancia(id,
                "Angular para principiante",
                "descripcion",
                "url",
                ".../img.png",
                80,
                2500.0,
                CategoriaFactory.factoryCategoria(1));
    }

    public static List<Curso> listaDeCursos(Integer cantidad) {
        List<Curso> retorno = new ArrayList<>();
        for (int i = 0; i < cantidad; i++) {
            retorno.add(factoryCurso(i));
        }
        return retorno;
    }
}
