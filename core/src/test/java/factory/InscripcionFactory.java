package factory;

import inscripcion.model.Inscripcion;

import java.util.ArrayList;
import java.util.List;

public class InscripcionFactory {

    public InscripcionFactory() {
    }

    public static Inscripcion factoryInscripcion(Integer id) {
        return Inscripcion.instancia(id, PersonaFactory.factoryPersona(1), CursoFactory.factoryCurso(1), "observaciones");
    }

    public static Inscripcion factoryInscripcionPersonaID1(Integer id) {
        return Inscripcion.instancia(id, PersonaFactory.factoryPersona(1), CursoFactory.factoryCurso(id), "observaciones");
    }

    public static List<Inscripcion> listaDeCursosDePersonaID1(Integer cantidad) {
        List<Inscripcion> retorno = new ArrayList<>();
        for (int i = 0; i < cantidad; i++) {
            retorno.add(factoryInscripcionPersonaID1(i));
        }
        return retorno;
    }
}
