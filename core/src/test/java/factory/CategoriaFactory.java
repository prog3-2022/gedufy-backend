package factory;

import categoria.model.Categoria;

public class CategoriaFactory {
    private CategoriaFactory() {
    }

    public static Categoria factoryCategoria(Integer id) {
        return Categoria.instancia(id, "Programación", "descripcion");
    }
}
