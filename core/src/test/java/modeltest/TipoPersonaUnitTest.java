package modeltest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tipopersona.exception.TipoPersonaIncompletoException;
import tipopersona.model.TipoPersona;

public class TipoPersonaUnitTest {

    // nombre metodo: WUT_escenario_Resultado
    @Test
    public void instanciar_atributosCorrectos_Instancia() {
        // Arrange

        // Act
        TipoPersona tipoPersona = TipoPersona.instancia(1, "Alumno", "descripcion");

        // Assert
        Assertions.assertNotNull(tipoPersona);
    }

    @Test
    public void instanciar_faltaNombre_TipoPersonaIncompletoException() {
        Exception exceptionVacio = Assertions.assertThrows(TipoPersonaIncompletoException.class, () -> TipoPersona.instancia(1, "", "descripcion"));
        Exception exceptionNulo = Assertions.assertThrows(TipoPersonaIncompletoException.class, () -> TipoPersona.instancia(1, null, "descripcion"));
        Assertions.assertEquals("Falta nombre", exceptionVacio.getMessage());
        Assertions.assertEquals("Falta nombre", exceptionNulo.getMessage());
    }
}
