package modeltest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import persona.exception.PersonaIncompletoException;
import persona.model.Persona;
import tipopersona.model.TipoPersona;
import utils.exception.InvalidEmailException;

import java.time.LocalDate;

public class PersonaUnitTest {

    // nombre metodo: WUT_escenario_Resultado
    @Test
    public void instanciar_atributosCorrectos_Instancia() {
        // Arrange
        TipoPersona tipoPersona = TipoPersona.instancia(1, "Alumno", "descripcion");

        // Act
        Persona persona = Persona.instancia(1, "27444555", "Manu Ginobili", "manu@mail.com", "+543516112233", LocalDate.of(1980, 1, 20), tipoPersona);

        // Assert
        Assertions.assertNotNull(persona);
    }

    @Test
    public void instanciar_faltaNombre_PersonaIncompletoException() {
        TipoPersona tipoPersona = TipoPersona.instancia(1, "Alumno", "descripcion");

        Exception exceptionVacio = Assertions.assertThrows(PersonaIncompletoException.class, () -> Persona.instancia(1, "27444555", "", "manu@mail.com", "+543516112233", LocalDate.of(1980, 1, 20), tipoPersona));
        Exception exceptionNulo = Assertions.assertThrows(PersonaIncompletoException.class, () -> Persona.instancia(1, "27444555", null, "manu@mail.com", "+543516112233", LocalDate.of(1980, 1, 20), tipoPersona));
        Assertions.assertEquals("Falta nombre", exceptionVacio.getMessage());
        Assertions.assertEquals("Falta nombre", exceptionNulo.getMessage());
    }

    @Test
    public void instanciar_faltaEmail_PersonaIncompletoException() {
        TipoPersona tipoPersona = TipoPersona.instancia(1, "Alumno", "descripcion");

        Exception exceptionVacio = Assertions.assertThrows(PersonaIncompletoException.class, () -> Persona.instancia(1, "27444555", "Manu Ginobili", "", "+543516112233", LocalDate.of(1980, 1, 20), tipoPersona));
        Exception exceptionNulo = Assertions.assertThrows(PersonaIncompletoException.class, () -> Persona.instancia(1, "27444555", "Manu Ginobili", null, "+543516112233", LocalDate.of(1980, 1, 20), tipoPersona));
        Assertions.assertEquals("Falta email", exceptionVacio.getMessage());
        Assertions.assertEquals("Falta email", exceptionNulo.getMessage());
    }

    @Test
    public void instanciar_faltaTipoPersona_PersonaIncompletoException() {
        Exception exceptionNulo = Assertions.assertThrows(PersonaIncompletoException.class, () -> Persona.instancia(1, "27444555", "Manu Ginobili", "manu@mail.com", "+543516112233", LocalDate.of(1980, 1, 20), null));
        Assertions.assertEquals("Falta tipo de persona", exceptionNulo.getMessage());
    }

    @Test
    public void instanciar_invalidEmail_InvalidEmailException() {
        TipoPersona tipoPersona = TipoPersona.instancia(1, "Alumno", "descripcion");

        Exception exceptionInvalidEmail = Assertions.assertThrows(InvalidEmailException.class, () -> Persona.instancia(1, "27444555", "Manu Ginobili", "hola", "+543516112233", LocalDate.of(1980, 1, 20), tipoPersona));
        Assertions.assertEquals("Email inválido", exceptionInvalidEmail.getMessage());
    }
}
