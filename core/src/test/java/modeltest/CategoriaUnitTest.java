package modeltest;

import categoria.exception.CategoriaIncompletoException;
import categoria.model.Categoria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CategoriaUnitTest {

    @Test
    public void instanciar_atributosCorrectos_Instancia() {
        // Arrange

        // Act
        Categoria categoria = Categoria.instancia(1, "Programación", "descripcion");

        // Assert
        Assertions.assertNotNull(categoria);
    }

    @Test
    public void instanciar_faltaNombre_CategoriaIncompletoException() {
        Exception exceptionVacio = Assertions.assertThrows(CategoriaIncompletoException.class, () -> Categoria.instancia(1, "", "descripcion"));
        Exception exceptionNulo = Assertions.assertThrows(CategoriaIncompletoException.class, () -> Categoria.instancia(1, null, "descripcion"));
        Assertions.assertEquals("Falta nombre", exceptionVacio.getMessage());
        Assertions.assertEquals("Falta nombre", exceptionNulo.getMessage());
    }
}
