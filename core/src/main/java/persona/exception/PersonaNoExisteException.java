package persona.exception;

public class PersonaNoExisteException extends RuntimeException {
    public PersonaNoExisteException(String message) {
        super(message);
    }
}
