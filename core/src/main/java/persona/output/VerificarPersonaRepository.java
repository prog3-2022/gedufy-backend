package persona.output;

public interface VerificarPersonaRepository {
    boolean existePersonaPorEmail(String email);
}
