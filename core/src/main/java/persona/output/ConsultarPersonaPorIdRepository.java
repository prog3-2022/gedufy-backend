package persona.output;

import persona.model.Persona;

public interface ConsultarPersonaPorIdRepository {

    Persona consultarPersonaPorId(Integer id);
}
