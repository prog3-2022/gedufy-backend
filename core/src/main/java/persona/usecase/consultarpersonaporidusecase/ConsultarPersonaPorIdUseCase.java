package persona.usecase.consultarpersonaporidusecase;

import persona.input.ConsultarPersonaPorIdInput;
import persona.model.Persona;
import persona.output.ConsultarPersonaPorIdRepository;

public class ConsultarPersonaPorIdUseCase implements ConsultarPersonaPorIdInput {

    private ConsultarPersonaPorIdRepository consultarPersonaPorIdRepository;

    public ConsultarPersonaPorIdUseCase(ConsultarPersonaPorIdRepository consultarPersonaPorIdRepository) {
        this.consultarPersonaPorIdRepository = consultarPersonaPorIdRepository;
    }

    @Override
    public Persona consultarPersonaPorId(Integer id) {
        return consultarPersonaPorIdRepository.consultarPersonaPorId(id);
    }
}
