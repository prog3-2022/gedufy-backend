package persona.usecase.crearpersonausecase;

import persona.exception.PersonaYaExisteException;
import persona.input.CrearPersonaInput;
import persona.model.Persona;
import persona.output.CrearPersonaRepository;
import tipopersona.input.ConsultarTipoPersonaPorIdInput;

public class CrearPersonaUseCase implements CrearPersonaInput {
    private CrearPersonaRepository crearPersonaRepository;
    private ConsultarTipoPersonaPorIdInput consultarTipoPersonaPorIdInput;

    public CrearPersonaUseCase(CrearPersonaRepository crearPersonaRepository, ConsultarTipoPersonaPorIdInput consultarTipoPersonaPorIdInput) {
        this.crearPersonaRepository = crearPersonaRepository;
        this.consultarTipoPersonaPorIdInput = consultarTipoPersonaPorIdInput;
    }

    @Override
    public Integer crearPersona(CrearPersonaRequestModel crearPersonaRequestModel) throws PersonaYaExisteException {
        if (crearPersonaRepository.existePorDNI(crearPersonaRequestModel.getDni())) {
            throw new PersonaYaExisteException(String.format("Ya existe persona con dni %s", crearPersonaRequestModel.getDni()));
        }
        if (crearPersonaRepository.existePorEmail(crearPersonaRequestModel.getEmail())) {
            throw new PersonaYaExisteException(String.format("Ya existe persona con email %s", crearPersonaRequestModel.getEmail()));
        }

        Persona nuevaPersona = Persona.instancia(null,
                crearPersonaRequestModel.getDni(),
                crearPersonaRequestModel.getNombre(),
                crearPersonaRequestModel.getEmail(),
                crearPersonaRequestModel.getTelefono(),
                crearPersonaRequestModel.getFecha_nacimiento(),
                consultarTipoPersonaPorIdInput.consultarTipoPersonaPorId(crearPersonaRequestModel.getTipo_persona_id()));

        return crearPersonaRepository.guardarPersona(nuevaPersona);
    }
}
