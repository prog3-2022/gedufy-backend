package persona.input;

import persona.model.Persona;

public interface ConsultarPersonaPorIdInput {

    Persona consultarPersonaPorId(Integer id);
}
