package categoria.model;

import categoria.exception.CategoriaIncompletoException;

public class Categoria {
    private Integer id;
    private String nombre;
    private String descripcion;

    private Categoria(Integer id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public static Categoria instancia(Integer id, String nombre, String descripcion) {
        if (nombre == null || nombre.isEmpty()) {
            throw new CategoriaIncompletoException("Falta nombre");
        }
        return new Categoria(id, nombre, descripcion);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
