package categoria.exception;

public class CategoriaIncompletoException extends RuntimeException {
    public CategoriaIncompletoException(String message) {
        super(message);
    }
}
