package tipopersona.input;

import tipopersona.model.TipoPersona;

public interface ConsultarTipoPersonaPorIdInput {

    TipoPersona consultarTipoPersonaPorId(Integer tipoPersonaId);
}
