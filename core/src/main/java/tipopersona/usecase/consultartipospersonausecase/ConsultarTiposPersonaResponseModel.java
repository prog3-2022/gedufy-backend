package tipopersona.usecase.consultartipospersonausecase;

import tipopersona.exception.TipoPersonaMapeoResponseException;

public class ConsultarTiposPersonaResponseModel {
    private Integer id;
    private String nombre;


    private ConsultarTiposPersonaResponseModel() {
    }

    private ConsultarTiposPersonaResponseModel(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public static ConsultarTiposPersonaResponseModel instancia(Integer id, String nombre) {
        try {
            ConsultarTiposPersonaResponseModel consultarTiposPersonaResponseModel = new ConsultarTiposPersonaResponseModel(
                    id,
                    nombre);
            return consultarTiposPersonaResponseModel;
        } catch (Exception e) {
            throw new TipoPersonaMapeoResponseException("Error al crear instancia de ConsultarTiposPersonaResponseModel");
        }
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
