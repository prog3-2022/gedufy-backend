package tipopersona.exception;

public class TipoPersonaMapeoResponseException extends RuntimeException {
    public TipoPersonaMapeoResponseException(String message) {
        super(message);
    }
}
