package tipopersona.output;

import tipopersona.model.TipoPersona;

public interface ConsultarTipoPersonaPorIdRepository {

    TipoPersona consultarTipoPersonaPorId(Integer tipoPersonaId);
}
