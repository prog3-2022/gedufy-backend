package curso.usecase.consultarcursoporidusecase;

import curso.input.ConsultarCursoPorIdInput;
import curso.model.Curso;
import curso.output.ConsultarCursoPorIdRepositorio;

public class ConsultarCursoPorIdUseCase implements ConsultarCursoPorIdInput {
    private ConsultarCursoPorIdRepositorio consultarCursoPorIdRepositorio;

    public ConsultarCursoPorIdUseCase(ConsultarCursoPorIdRepositorio consultarCursoPorIdRepositorio) {
        this.consultarCursoPorIdRepositorio = consultarCursoPorIdRepositorio;
    }

    @Override
    public ConsultarCursoPorIdResponseModel obtenerCursoPorId(Integer id) {
        Curso curso = consultarCursoPorIdRepositorio.obtenerCursoPorId(id);
        ConsultarCursoPorIdResponseModel consultarCursoPorIdResponseModel = ConsultarCursoPorIdResponseModel.instancia(
                curso.getId(),
                curso.getNombre(),
                curso.getUrl(),
                curso.getImagen(),
                curso.getCantHoras(),
                curso.getPrecio(),
                curso.getCategoria().getNombre(),
                curso.getDescripcion());

        return consultarCursoPorIdResponseModel;
    }

    @Override
    public Curso obtenerCursoCorePorId(Integer id) {
        return consultarCursoPorIdRepositorio.obtenerCursoPorId(id);
    }
}
