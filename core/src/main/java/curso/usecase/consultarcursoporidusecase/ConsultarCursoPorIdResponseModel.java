package curso.usecase.consultarcursoporidusecase;

import curso.exception.CursoMapeoResponseException;

public class ConsultarCursoPorIdResponseModel {
    private Integer id;
    private String nombre;
    private String url;
    private String imagen;
    private Integer cantHoras;
    private Double precio;
    private String categoria;
    private String descripcion;

    private ConsultarCursoPorIdResponseModel() {
    }

    private ConsultarCursoPorIdResponseModel(Integer id, String nombre, String url, String imagen, Integer cantHoras, Double precio, String categoria, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.url = url;
        this.imagen = imagen;
        this.cantHoras = cantHoras;
        this.precio = precio;
        this.categoria = categoria;
        this.descripcion = descripcion;
    }

    public static ConsultarCursoPorIdResponseModel instancia(Integer id, String nombre, String url, String imagen, Integer cantHoras, Double precio, String categoria, String descripcion) {
        try {
            ConsultarCursoPorIdResponseModel consultarCursosResponseModel = new ConsultarCursoPorIdResponseModel(
                    id,
                    nombre,
                    url,
                    imagen,
                    cantHoras,
                    precio,
                    categoria,
                    descripcion);
            return consultarCursosResponseModel;
        } catch (Exception e) {
            throw new CursoMapeoResponseException("Error al crear instancia de ConsultarCursoPorIdResponseModel");
        }
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getUrl() {
        return url;
    }

    public String getImagen() {
        return imagen;
    }

    public Integer getCantHoras() {
        return cantHoras;
    }

    public Double getPrecio() {
        return precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
