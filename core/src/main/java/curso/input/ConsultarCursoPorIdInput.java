package curso.input;

import curso.model.Curso;
import curso.usecase.consultarcursoporidusecase.ConsultarCursoPorIdResponseModel;

public interface ConsultarCursoPorIdInput {
    ConsultarCursoPorIdResponseModel obtenerCursoPorId(Integer id);

    Curso obtenerCursoCorePorId(Integer id);

}

