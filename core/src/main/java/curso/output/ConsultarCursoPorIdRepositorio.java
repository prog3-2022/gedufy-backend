package curso.output;

import curso.model.Curso;

public interface ConsultarCursoPorIdRepositorio {
    Curso obtenerCursoPorId(Integer id);
}
