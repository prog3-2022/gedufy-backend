package curso.output;

import curso.model.Curso;

import java.util.Collection;

public interface ConsultarCursosRepositorio {
    Collection<Curso> obtenerCursos();
}
