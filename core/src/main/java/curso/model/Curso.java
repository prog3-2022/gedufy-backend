package curso.model;

import categoria.model.Categoria;
import curso.exception.CursoIncompletoException;
import persona.model.Persona;

public class Curso {
    private Integer id;
    private String nombre;
    private String descripcion;
    private String url;
    private String imagen;
    private Integer cantHoras;
    private Double precio;
    private Categoria categoria;

    private Curso(Integer id, String nombre, String descripcion, String url, String imagen, Integer cantHoras, Double precio, Categoria categoria) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.url = url;
        this.imagen = imagen;
        this.cantHoras = cantHoras;
        this.precio = precio;
        this.categoria = categoria;
    }

    public static Curso instancia(Integer id, String nombre, String descripcion, String url, String imagen, Integer cantHoras, Double precio, Categoria categoria) {
        if (nombre == null || nombre.isEmpty()) {
            throw new CursoIncompletoException("Falta nombre");
        }
        if (imagen == null || imagen.isEmpty()) {
            throw new CursoIncompletoException("Falta imagen");
        }
        if (precio == null) {
            throw new CursoIncompletoException("Falta precio");
        }
        if (categoria == null) {
            throw new CursoIncompletoException("Falta categoria");
        }
        return new Curso(id, nombre, descripcion, url, imagen, cantHoras, precio, categoria);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Integer getCantHoras() {
        return cantHoras;
    }

    public void setCantHoras(Integer cantHoras) {
        this.cantHoras = cantHoras;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}
