package inscripcion.usecase.crearinscripcionusecase;

public class CrearInscripcionRequestModel {

    private Integer id_persona;
    private Integer id_curso;
    private String observaciones;

    private CrearInscripcionRequestModel() {
    }

    public CrearInscripcionRequestModel(Integer id_persona, Integer id_curso, String observaciones) {
        this.id_persona = id_persona;
        this.id_curso = id_curso;
        this.observaciones = observaciones;
    }

    public static CrearInscripcionRequestModel instancia(Integer id_persona, Integer id_curso, String observaciones) {
        try {
            CrearInscripcionRequestModel crearPersonaRequestModel = new CrearInscripcionRequestModel(
                    id_persona,
                    id_curso,
                    observaciones);
            return crearPersonaRequestModel;
        } catch (Exception e) {
            throw new RuntimeException("Error al crear instancia de CrearInscripcionRequestModel");
        }
    }

    public Integer getId_persona() {
        return id_persona;
    }

    public void setId_persona(Integer id_persona) {
        this.id_persona = id_persona;
    }

    public Integer getId_curso() {
        return id_curso;
    }

    public void setId_curso(Integer id_curso) {
        this.id_curso = id_curso;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
