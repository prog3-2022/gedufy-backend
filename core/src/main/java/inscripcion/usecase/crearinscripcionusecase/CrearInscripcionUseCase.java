package inscripcion.usecase.crearinscripcionusecase;

import curso.input.ConsultarCursoPorIdInput;
import inscripcion.exception.InscripcionYaExisteException;
import inscripcion.input.CrearInscripcionInput;
import inscripcion.model.Inscripcion;
import inscripcion.output.CrearInscripcionRepository;
import persona.input.ConsultarPersonaPorIdInput;

public class CrearInscripcionUseCase implements CrearInscripcionInput {

    private CrearInscripcionRepository crearInscripcionRepository;
    private ConsultarPersonaPorIdInput consultarPersonaPorIdInput;
    private ConsultarCursoPorIdInput consultarCursoPorIdInput;

    public CrearInscripcionUseCase(CrearInscripcionRepository crearInscripcionRepository,
                                   ConsultarPersonaPorIdInput consultarPersonaPorIdInput,
                                   ConsultarCursoPorIdInput consultarCursoPorIdInput) {
        this.crearInscripcionRepository = crearInscripcionRepository;
        this.consultarPersonaPorIdInput = consultarPersonaPorIdInput;
        this.consultarCursoPorIdInput = consultarCursoPorIdInput;
    }

    @Override
    public Integer crearInscripcion(CrearInscripcionRequestModel crearInscripcionRequestModel) throws InscripcionYaExisteException {

        if (crearInscripcionRepository.existePorPersonaAndCurso(
                crearInscripcionRequestModel.getId_persona(),
                crearInscripcionRequestModel.getId_curso())) {
            throw new InscripcionYaExisteException("Ya existe la inscripcion");
        }

        Inscripcion nuevaInscripcion = Inscripcion.instancia(null,
                consultarPersonaPorIdInput.consultarPersonaPorId(crearInscripcionRequestModel.getId_persona()),
                consultarCursoPorIdInput.obtenerCursoCorePorId(crearInscripcionRequestModel.getId_curso()),
                crearInscripcionRequestModel.getObservaciones());

        return crearInscripcionRepository.guardarInscripcion(nuevaInscripcion);
    }
}
