package inscripcion.usecase;

import curso.model.Curso;
import inscripcion.input.ConsultarInscripcionesInput;
import inscripcion.model.Inscripcion;
import inscripcion.output.ConsultarInscripcionesRepository;

import java.util.List;
import java.util.stream.Collectors;

public class ConsultarInscripcionesUseCase implements ConsultarInscripcionesInput {

    ConsultarInscripcionesRepository consultarInscripcionesRepository;

    public ConsultarInscripcionesUseCase(ConsultarInscripcionesRepository consultarInscripcionesRepository) {
        this.consultarInscripcionesRepository = consultarInscripcionesRepository;
    }

    @Override
    public List<Curso> obtenerInscripciones(Integer personaId) {
        return consultarInscripcionesRepository.obtenerInscripciones(personaId)
                .stream().map(Inscripcion::getCurso)
                .collect(Collectors.toList());
    }
}
