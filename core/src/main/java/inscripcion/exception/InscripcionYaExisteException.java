package inscripcion.exception;

public class InscripcionYaExisteException extends RuntimeException {

    public InscripcionYaExisteException(String message) {
        super(message);
    }
}
