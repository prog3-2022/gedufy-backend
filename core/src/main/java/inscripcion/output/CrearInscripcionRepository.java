package inscripcion.output;

import inscripcion.model.Inscripcion;

public interface CrearInscripcionRepository {

    boolean existePorPersonaAndCurso(Integer personaId, Integer cursoId);

    Integer guardarInscripcion(Inscripcion inscripcion);
}
