package inscripcion.input;

import inscripcion.usecase.crearinscripcionusecase.CrearInscripcionRequestModel;

public interface CrearInscripcionInput {
    Integer crearInscripcion(CrearInscripcionRequestModel crearInscripcionRequestModel);
}
